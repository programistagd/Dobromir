var userPackage = {
    init : function(){
        /*var c = function(res){
            return res.text();
        };
        var p1 = addTimeout(fetchCached('googl','http://google.pl',c,1,true),1000);
        app.log(p1);
        var prt = function(r){
            app.log("Promise: "+r.state+" - "+(r.state == 'fulfilled' ? r.value : r.reason));
        };
        RSVP.allSettled([p1]).then(function(r){
            prt(r[0]);
            //prt(r[1]);
            app.log("Now is "+new Date());
        }).settled(function(){
          app.log("All done");  
        });*/
        /*var wait = function(text){
            return new Promise(function(resolve,reject){
                setTimeout(function(){
                    resolve(text);
                },1000);
            });
        };
        speakPromiseChain([wait("test"),wait("syntezatora"),wait("mowy")]);*/
        
        $("#say").click(function(){
            speakOnce($("#tts").val());
        });
        $("#cal").click(function(){
            getTodayEvents().then(function(events){
                app.log("events:");
                app.log(JSON.stringify(events));
            }).catch(function(err){
                app.log("error: "+err);
            });
        });
        $("#wake").click(function(){
            app.log("Trying to schedule...");
            var now = new Date();
            scheduleWakeup(now.getHours(),now.getMinutes()+2).then(function(){
                app.log("Scheduled wakeup");  
            }).catch(function(err){
                app.log("Error scheduling: "+err);  
            });
        });
        $("#doonce").click(function(){
            doOnceIn("doonce",10).then(function(){
                alert("HI!");
            });
        });
        $("#daily").click(function(){
            doOnceDaily("day").then(function(){
                alert("Good morning!");
            });
        });
        $("#resetd").click(function(){
            resetDailyTimer("day");
        });
        locateMe().then(function(loc){
            app.log(loc.coords.latitude+", "+loc.coords.longitude);
        });
        delay(150).then(function(){speakOnce("OK.");});
    },
    wakeup : function(data){
        app.log("wakeup");
        app.log(data);
    }
};
window.userPackage = userPackage;
