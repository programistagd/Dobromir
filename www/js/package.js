var PackageManager = {
    removeOld : function(){
        //remove old package files
        return new Promise(function(resolve, reject){
            var callback = function(fs){
                fs.root.getDirectory(
                    "package",
                    {create : true, exclusive : false},
                    function(entry) {
                        entry.removeRecursively(function() {
                            resolve();
                        }, reject);
                    }, reject);
            };
            window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, callback, reject);
        });
    },
    downloadPackage : function(url){
        var PM = PackageManager;
        PM.removeOld().then(function(){
            app.log("Downloading from "+url);
            var cacheDestroyer = (new Date()).getTime();
            fetch(url+"Manifest.json?t="+cacheDestroyer,{cache: "no-store"}).then(function(r){
                return r.json();
            }).then(function(data){
                //app.log(JSON.stringify(data));
                app.log("Downloading package \""+data["name"]+"\", version "+data["version"]);
                var files = [];
                files.push(PM.downloadFile(url+data["index"], "main.html"));
                for(var i = 0; i < data["scripts"].length; ++i){
                    files.push(PM.downloadFile(url+data["scripts"][i], "scr" + i + ".js"));
                }
                window.localStorage.setItem("scripts",data["scripts"].length);
                for(var i = 0; i < data["css"].length; ++i){
                    files.push(PM.downloadFile(url+data["css"][i], "stl" + i + ".css"));
                }
                window.localStorage.setItem("css",data["css"].length);
                for(var i = 0; i < data["resources"].length; ++i){
                    files.push(PM.downloadFile(url+data["resources"][i], data["resources"][i]));
                }
                app.log("Downloading "+files.length+" files...");
                RSVP.all(files).then(function(entries){
                    PM.initPackage();
                }).catch(function(error){
                    app.log("Update failed: "+error+JSON.stringify(error));
                    app.log(JSON.Stringify(error));
                    app.log("No recovery available, you have to install original package again.");//TODO before removing old package try to download new to other directory and then replace old files
                    window.localStorage.setItem("scripts",0);//disable runtime as we are in bad state :(
                });
            });
        });
    },
    downloadFile : function(url, name){
        return new Promise(function(resolve,reject){
            var fileTransfer = new FileTransfer();
            var uri = encodeURI(url);

            fileTransfer.download(
                uri,
                "cdvfile://localhost/persistent/package/"+name,
                function(entry) {
                    app.log("Downloaded "+url);
                    resolve(entry);
                    //console.log("download complete: " + entry.toURL());
                },
                function(error) {
                    app.log("Error downloading "+url+", "+error.code);
                    reject(error);
                    //console.log("download error source " + error.source);
                    //console.log("download error target " + error.target);
                    //console.log("download error code" + error.code);
                },
                false
            );
        });
    },
    loadScript : function(path){
        return new Promise(function(resolve, reject){
            $.getScript(path).done(resolve).fail(reject);
        });
    },
    initPackage : function(){
        //load scripts from local storage and run them
        var fail = function(err){
            app.log("Package initialization failed: "+err+", "+JSON.stringify(err));
            $("#view").html(app.getReadme());
        };
        var scripts = parseInt(window.localStorage.getItem("scripts"));
        if(scripts === null || isNaN(scripts) || scripts === 0){
            fail("No correct package in storage");
            return;
        }
        var css = window.localStorage.getItem("css");
        if(css === null){
            css = 0;
        }
        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fs){
           fs.root.getFile("package/main.html", { create: false, exclusive: false }, function (fileEntry) {
               fileEntry.file(function (file) {
                   var reader = new FileReader();
                   reader.onloadend = function() {
                       $('link').remove();
                       for(var i=0;i<css;++i){
                           $('head').append('<link rel="stylesheet" type="text/css" href="cdvfile://localhost/persistent/package/stl'+i+'.css">');
                       }
                       $("#view").html(this.result);
                       var scrs = [];
                       for(var i = 0; i < scripts; ++i){
                            scrs.push(PackageManager.loadScript("cdvfile://localhost/persistent/package/scr"+i+".js"));
                       }
                       RSVP.all(scrs).then(function(){
                            window.userPackage.init();//run package init script
                       });
                   };
                   reader.readAsText(file);
               }, fail);
           }, fail); 
        },fail);
    }
};
