var delay = function(ms){
    return new Promise(function(fulfill,reject){
        setTimeout(fulfill,ms);
    });
};

//returns a promise that fails after timeout is exceeded unless the original promise has been fulfilled in time
var addTimeout = function(promise, ms){
    return new Promise(function(fulfill, reject){
        promise.then(fulfill, reject);
        var timesout = function(){
            reject(new Error("Timeout ("+ms+"ms) exceeded"));
        };
        delay(ms).then(timesout).catch(timesout);
    });
};

//expects a JSON encoded object of format {t:Date} and returns null otherwise
function getDateOrNull(t){
    if(t !== null){
        try{
            t = JSON.parse(t);
            t = new Date(t.t);
        }
        catch(e){
            app.log(e);
            t = null;
        }
    }
    if(!(t instanceof Date)) return null;
    if(isNaN(t.getTime())) return null;
    return t;
}

//the returned promise only resolves if it hasn't been resolved in the last [seconds] seconds (identified by name), otherwise it rejects
var doOnceIn = function(name, seconds){
    name = "timing_"+name;
    return new Promise(function(resolve, reject){
        var t = window.localStorage.getItem(name);
        var now = new Date();
        t = getDateOrNull(t);
        if(t == null || (now - t)/1000 > seconds){
            resolve();
            window.localStorage.setItem(name,JSON.stringify({t:now}));
        }
        else{
            reject();
        }
    });
};

//returns a promise that resolves only once a day
var doOnceDaily = function(name){
    name = "daily_"+name;
    return new Promise(function(resolve, reject){
        var t = window.localStorage.getItem(name);
        var now = new Date();
        if(t != now.toDateString()){
            resolve();
            window.localStorage.setItem(name, now.toDateString());
        }
        else{
            reject();
        }
    });
};

//resets the timer associated with a given name (so the doOnceIn functions will surely resolve on next call
//useful for example when the function that was meant to be run failed and we'd like to try again next time
var resetOnceInTimer = function(name){
    window.localStorage.setItem("timing_"+name,0);
};

var resetDailyTimer = function(name){
    window.localStorage.setItem("daily_"+name,0);
};
//schedules a one-time wakeup and fulfills to empty or rejects with error
var scheduleWakeup = function(hours, minutes, data){
    while(minutes>=60){
        minutes-=60;
        hours+=1;
    }
    hours = hours % 24;
    return new Promise(function(resolve,reject){
        var success = function(result){
            if(result == "OK" || result.type == 'set'){
                resolve();
            }
            else if(result.type == 'wakeup'){
                try{
                    window.userPackage.wakeup(result.extra);
                }catch(e){
                    
                }
            }
            else{
                app.log("Unknown wakeup callback "+result.type+", "+JSON.stringify(result));
            }
        };
        window.wakeuptimer.wakeup( success, reject, 
            {
                alarms: [{
                    type: 'onetime',
                    time: { hour: hours, minute: minutes },
                    //extra: data,
                }]
            });
    });
};
