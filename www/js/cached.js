/*
 * resource name in cache
 * resource url
 * function to process raw data to structure (result should be a promise returning a JSONifiable object)
 * time in minutes until cache needs to be refreshed
 * if promise should fail on refresh or use the old value 
 */
var fetchCached = function(name, url, process, freshness, shouldFail) {
    name = "ajaxcache_" + name;
    var v = window.localStorage.getItem(name);
    var now = new Date();
    if (v !== null) {
        try {
            v = JSON.parse(v);
            v.timestamp = new Date(v.timestamp);
        } catch (e) {
            window.onerror(e, "cached.js", 0, 0, e);
            v = null;
        }
    }
    if (v === null || isNaN(now - v.timestamp) || (now - v.timestamp) / (1000 * 60) > freshness) {
        return new Promise(function(resolve, reject) {
            fetch(url).then(function(r) {
                if (r.ok) {
                    process(r).then(function(data) {
                        var memento = {
                            timestamp: now,
                            data: data
                        };
                        window.localStorage.setItem(name, JSON.stringify(memento));
                        resolve(data);
                    }).catch(function(err) {
                        reject(err);
                    });
                } else {
                    reject(new Error("Request returned status " + r.status + " " + r.statusText));
                }
            }).catch(function(e) {
                if (v === null || shouldFail) {
                    reject(e);
                } else {
                    resolve(v.data);
                }
            });
        });
    } else {
        return new Promise(function(resolve, reject) {
            resolve(v.data);
        });
    }
};