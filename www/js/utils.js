RSVP.Promise.prototype.settled = function(callback){
    RSVP.allSettled([this]).then(function(r){
        callback(r[0]);
    });
};

function emptyPromise(){
    return new Promise(function(resolve,reject){
        resolve();
    });
}
