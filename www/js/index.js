/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    log: function(s) {
        $("#info").html($("#info").html() + s + "<br>");
        $("#clearlog").show();
    },
    debugLog: function(s) {
        //TODO debug switch
        $("#info").html($("#info").html() + s + "<br>");
        $("#clearlog").show();
    },
    clearLog: function() {
        $("#info").html("");
        $("#clearlog").hide();
    },
    getReadme: function() {
        var html = "TODO: format<br>This is only a package manager with some APIs making it easier to create a customized personal assistant app.<br>" +
            "To have any useful functionality you need to install some package<br>" +
            "You can find an example package at <a href=\"http://github.com/\">TODO</a><br>" +
            "Here you can find documentation of APIs available (TODO)";
        return html;
    },
    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
        window.app = this;
        window.onerror = function(msg, url, line, col, err) {
            //TODO special DIV for errors, with clickable clear
            app.log("JS Err: " + msg + " @" + url + ":" + line + ":" + col);
        };
        //app.log("Ready");
        $("#clearlog").click(app.clearLog);
        try {
            navigator.app.overrideButton("menubutton", true);
        } catch (e) {

        }
        document.addEventListener("resume", this.onResume);
        document.addEventListener("menubutton", function(e) {
            var url = window.localStorage.getItem("repoUrl");
            if (url == null) {
                url = "";
            }
            url = prompt("Repository URL", url);
            if (url === null) return;
            window.localStorage.setItem("repoUrl", url);
            PackageManager.downloadPackage(url);
        }, false);
        PackageManager.initPackage();
    },
    onResume: function() {
        try {
            window.userPackage.resume();
        } catch (e) {
            app.log(e);
        }
    }
};

app.initialize();