function speakOnce(text){
    return new Promise(function(resolve, reject){
        window.TTS.speak({
                text: text,
                locale: 'pl-PL',
                rate: 1
            },resolve,reject);
    });
}

function speakPromiseChain(promises){
    var speak = function(previousSpeech, i){
        if(i >= promises.length){
            return;
        }
        var newText = promises[i];
        RSVP.allSettled([previousSpeech, newText]).then(function(res){
            var newSpeech = emptyPromise();
            if(res[1].state == 'fulfilled'){
                newSpeech = speakOnce(res[1].value);
            }
            speak(newSpeech, i+1);
        });
    };
    speak(emptyPromise(), 0);
}
