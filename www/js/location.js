//return a promise returning current location coords and timestamp
function locateMe(){
    //TODO maybe add some parameters?
    return new Promise(function(resolve, reject){
        navigator.geolocation.getCurrentPosition(resolve, reject, { timeout: 5000, enableHighAccuracy: true });
    });
}

//location predicate is a function of the form (coords => Boolean)

function intersect(p1, p2){
    return function(coords){
        return (p1(coords) && p2(coords));
    }
}

function union(p1, p2){
    return function(coords){
        return (p1(coords) || p2(coords));
    }
}

//cases is an array of {p,f} where p is a location is predicate and f is a function
//locations are tested in order and the first one is run
//if none matches, fallback function is run
function locationSwitch(cases, fallback){
    locateMe().then(function(loc){
        for(var i=0;i<cases.length;i++){
            if(cases[i].p(loc.coords)){
                cases[i].f();
                return;
            }
        }
        fallback();
    }).catch(fallback);
}
