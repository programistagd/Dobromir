function getTodayEvents(){
    return new Promise(function(resolve, reject){
        var b = new Date();
        var e = new Date();
        b.setHours(0,0,0,0);//midnight today
        e.setHours(0,0,0,0);
        e.setDate(e.getDate()+1);//midnight tomorrow
        window.plugins.calendar.listEventsInRange(b,e,resolve,reject);
        //I would also like include events that are longer than one day - but no simple solution seems to be available -> consider forking the plugin and adding a query in native API
    });
};

function getTomorrowEvents(){
    return new Promise(function(resolve, reject){
        var b = new Date();
        var e = new Date();
        b.setHours(0,0,0,0);
        e.setHours(0,0,0,0);
        b.setDate(b.getDate()+1);//midnight tomorrow
        e.setDate(e.getDate()+2);//midnight the next day
        window.plugins.calendar.listEventsInRange(b,e,resolve,reject);
    });
};
//all for now -> specific needs can be accessed by using plugins.calendar directly

